package com.cgi.usermanagement.rest;

import com.cgi.usermanagement.persistence.entities.PersonInfo;
import com.cgi.usermanagement.persistence.entities.PersonBasic;
import com.cgi.usermanagement.persistence.entities.Role;
import com.cgi.usermanagement.service.PersonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "/persons")
@RestController
@RequestMapping("/persons")
public class PersonRestController {
    private PersonService personService;

    @Autowired
    public PersonRestController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping(path = "/{id}")
    @ApiOperation(value = "Get Person by id",
            produces = MediaType.APPLICATION_JSON_VALUE,
            httpMethod = "GET",
            response = PersonBasic.class)
    public ResponseEntity<PersonBasic> getPersonById(@PathVariable Long id) {
        return new ResponseEntity<>(personService.getPersonById(id), HttpStatus.OK);
    }

    @GetMapping(path = "/email/{email}")
    @ApiOperation(value = "Get Person by email",
            produces = MediaType.APPLICATION_JSON_VALUE,
            httpMethod = "GET",
            response = PersonBasic.class)
    public ResponseEntity<PersonBasic> getPersonByEmail(@PathVariable String email) {
        return new ResponseEntity<>(personService.getPersonByEmail(email), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}/roles")
    @ApiOperation(value = "Get Person's roles by id",
            produces = MediaType.APPLICATION_JSON_VALUE,
            httpMethod = "GET",
            response = Role.class,
            responseContainer = "List")
    public ResponseEntity<List<Role>> getRolesById(@PathVariable Long id){
        return new ResponseEntity<>(personService.getRolesById(id), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/info")
    @ApiOperation(value = "Get Person's info by id",
            notes = "Requires a manager role",
            produces = MediaType.APPLICATION_JSON_VALUE,
            httpMethod = "GET",
            response = PersonInfo.class)
    public ResponseEntity<PersonInfo> getInfoById(@PathVariable Long id) {
        return new ResponseEntity<>(personService.getPersonInfo(id), HttpStatus.OK);
    }

}
