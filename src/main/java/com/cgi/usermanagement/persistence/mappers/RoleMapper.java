package com.cgi.usermanagement.persistence.mappers;

import com.cgi.usermanagement.persistence.entities.Role;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RoleMapper implements RowMapper {

    @Override
    public Role mapRow(ResultSet resultSet, int i) throws SQLException {
        Role role = new Role();
        role.setId(resultSet.getLong("id"));
        role.setType(resultSet.getString("role_type"));
        role.setDescription(resultSet.getString("description"));
        return role;
    }
}
