package com.cgi.usermanagement.persistence.mappers;

import com.cgi.usermanagement.persistence.entities.Address;
import com.cgi.usermanagement.persistence.entities.Person;
import com.cgi.usermanagement.persistence.entities.PersonInfo;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class InfoMapper implements RowMapper {
    @Override

    public PersonInfo mapRow(ResultSet resultSet, int i) throws SQLException {
        PersonInfo personInfo = new PersonInfo();
        Address address = new Address();
        Person person = new Person();

        person.setId(resultSet.getLong("id"));
        person.setName(resultSet.getString("name"));
        person.setEmail(resultSet.getString("email"));
        person.setPassword(resultSet.getString("password"));
        person.setSalary(resultSet.getInt("salary"));

        address.setCity(resultSet.getString("city"));
        address.setCountry(resultSet.getString("country"));
        address.setId(resultSet.getLong("address_id"));
        address.setPostcode(resultSet.getInt("postcode"));
        address.setStreet(resultSet.getString("street"));

        personInfo.setAddress(address);
        personInfo.setPerson(person);
        return personInfo;
    }
}
