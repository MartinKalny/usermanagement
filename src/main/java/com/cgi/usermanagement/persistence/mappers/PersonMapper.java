package com.cgi.usermanagement.persistence.mappers;

import com.cgi.usermanagement.persistence.entities.PersonBasic;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonMapper implements RowMapper {

    @Override
    public PersonBasic mapRow(ResultSet resultSet, int i) throws SQLException {
    PersonBasic person = new PersonBasic();
    person.setId(resultSet.getLong("id"));
    person.setName(resultSet.getString("name"));
    person.setEmail(resultSet.getString("email"));

    return person;
    }
}
