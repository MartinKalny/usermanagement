package com.cgi.usermanagement.persistence.mappers;

import com.cgi.usermanagement.persistence.entities.Group;
import com.cgi.usermanagement.persistence.entities.Role;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GroupMapper implements RowMapper {

    @Override
    public Group mapRow(ResultSet resultSet, int i) throws SQLException {
        Group group = new Group();
        Role role = new Role();
       group.setId(resultSet.getLong("id"));
       group.setName(resultSet.getString("name"));
       group.setDescription(resultSet.getString("description"));
       group.setExpirationDate(resultSet.getDate("expiration_date"));
       role.setId(resultSet.getLong("ro_id"));
       role.setType(resultSet.getString("role_type"));
       role.setDescription(resultSet.getString("role_description"));
       group.setRole(role);
        return group;
    }
}
