package com.cgi.usermanagement.persistence.repository;

import com.cgi.usermanagement.persistence.entities.*;
import com.cgi.usermanagement.persistence.mappers.GroupMapper;
import com.cgi.usermanagement.persistence.mappers.InfoMapper;
import com.cgi.usermanagement.persistence.mappers.PersonMapper;
import com.cgi.usermanagement.persistence.mappers.RoleMapper;
import com.cgi.usermanagement.service.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PersonRepository {
    private JdbcTemplate jdbcTemplate;
    String sqlBasic = "SELECT person.id, person.name, person.email FROM person WHERE ";
    String sqlAddress = "SELECT p.id, p.name, p.email, p.password, p.salary, p.address_id, ad.city, ad.country, " +
            "ad.street, ad.postcode FROM person p JOIN address ad ON p.address_id = ad.id WHERE ";

    @Autowired
    public PersonRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public PersonBasic getPersonById(Long id){
            String sql = sqlBasic + "id=?";
            return (PersonBasic) jdbcTemplate.queryForObject(sql, new Object[]{id}, new PersonMapper());
    }

    public PersonBasic getPersonByEmail(String email){
        String sql = sqlBasic + "email=?";
        return (PersonBasic) jdbcTemplate.queryForObject(sql, new Object[]{email}, new PersonMapper());
    }

    public PersonInfo getPersonInfo(Long id){
        String sql = sqlAddress + "p.id=?";
        PersonInfo personInfo = new PersonInfo();
        personInfo = (PersonInfo) jdbcTemplate.queryForObject(sql, new Object[]{id}, new InfoMapper());

        String sqlGroup = "SELECT pgr.id_group id, gr.name, gr.description, gr.expiration_date, ro.id ro_id, ro.role_type, " +
                "ro.description role_description FROM person p JOIN person_group_role pgr ON p.id = pgr.id_person " +
                "JOIN \"group\" gr ON pgr.id_group = gr.id JOIN \"role\" ro ON pgr.id_role = ro.id WHERE p.id = ?;";

        List<Group> groups=jdbcTemplate.query(sqlGroup, new Object[]{id}, new GroupMapper());
        personInfo.setGroupList(groups);
        return personInfo;
    }

    public PersonInfo getPersonInfoByEmail(String email){
        String sql = sqlAddress + "p.email=?";
        PersonInfo personInfo = new PersonInfo();
        personInfo = (PersonInfo) jdbcTemplate.queryForObject(sql, new Object[]{email}, new InfoMapper());

        String sqlGroup = "SELECT pgr.id_group id, gr.name, gr.description, gr.expiration_date, ro.id ro_id, ro.role_type, " +
                "ro.description role_description FROM person p JOIN person_group_role pgr ON p.id = pgr.id_person " +
                "JOIN \"group\" gr ON pgr.id_group = gr.id JOIN \"role\" ro ON pgr.id_role = ro.id WHERE p.email = ?;";

        List<Group> groups=jdbcTemplate.query(sqlGroup, new Object[]{email}, new GroupMapper());
        personInfo.setGroupList(groups);
        return personInfo;
    }


    public List<Role> getRolesById(Long id){
        String sql = "SELECT ro.id, ro.role_type, ro.description FROM person pe JOIN person_group_role pgr ON pe.id = pgr.id_person " +
                "JOIN \"group\" gr ON pgr.id_group = gr.id JOIN \"role\" ro ON pgr.id_role = ro.id WHERE pe.id = ?;";
        List<Role> roles= jdbcTemplate.query(sql, new Object[]{id}, new RoleMapper());
        return roles;
    }

}
