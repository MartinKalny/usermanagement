package com.cgi.usermanagement.persistence.entities;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

@ApiModel(value = "Group", description = "Details about a group")
public class Group {
    @ApiModelProperty(value = "Group ID", example = "1")
    Long id;
    @ApiModelProperty(value = "Group name", example = "name1")
    String name;
    @ApiModelProperty(value = "Group description", example = "description")
    String description;
    @ApiModelProperty(value = "Group expiration date", example = "2020-02-28T20:17:52")
    Date expirationDate;
    @ApiModelProperty(value = "Role of user in a group")
    Role role;

    public Group() {
    }

    public Group(Long id, String name, String description, Date expirationDate, Role role) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.expirationDate = expirationDate;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", expirationDate=" + expirationDate +
                ", role=" + role +
                '}';
    }
}
