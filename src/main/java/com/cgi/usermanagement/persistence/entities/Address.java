package com.cgi.usermanagement.persistence.entities;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Address", description = "Details about an address")
public class Address {
    @ApiModelProperty(value = "Address ID", example = "1")
    Long id;
    @ApiModelProperty(value = "City", example = "Prague")
    String city;
    @ApiModelProperty(value = "Country", example = "Czech Republic")
    String country;
    @ApiModelProperty(value = "Street", example = "New street")
    String street;
    @ApiModelProperty(value = "Postcode", example = "12345")
    int postcode;

    public Address() {
    }

    public Address(Long id, String city, String country, String street, int postcode) {
        this.id = id;
        this.city = city;
        this.country = country;
        this.street = street;
        this.postcode = postcode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getPostcode() {
        return postcode;
    }

    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", street='" + street + '\'' +
                ", postcode=" + postcode +
                '}';
    }
}
