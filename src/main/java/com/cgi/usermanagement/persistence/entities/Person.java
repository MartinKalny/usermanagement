package com.cgi.usermanagement.persistence.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Person", description = "Details about a person")
public class Person {
    @ApiModelProperty(value = "Person ID", example = "1")
    private Long id;
    @ApiModelProperty(value = "Person name", example = "name1")
    private String name;
    @ApiModelProperty(value = "Person email", example = "name1@email.com")
    private String email;
    @ApiModelProperty(value = "Person password", example = "password")
    private String password;
    @ApiModelProperty(value = "Person salary", example = "10000")
    private int salary;

    public Person() {
    }

    public Person(Long id, String name, String email, String password, int salary) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.salary = salary;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", salary=" + salary +
                '}';
    }

    public String toStringSimple() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

}
