package com.cgi.usermanagement.persistence.entities;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Role", description = "Person role")
public class Role {
    @ApiModelProperty(value = "Role ID", example = "1")
    Long id;
    @ApiModelProperty(value = "Role type", example = "manager")
    String type;
    @ApiModelProperty(value = "Role description", example = "manages")
    String description;

    public Role() {
    }

    public Role(Long id, String type, String description) {
        this.id = id;
        this.type = type;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
