package com.cgi.usermanagement.persistence.entities;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel(value = "PersonInfo", description = "Full information about a person")
public class PersonInfo {
    @ApiModelProperty(value = "Person info")
    private Person person;
    @ApiModelProperty(value = "Person address")
    private Address address;
    @ApiModelProperty(value = "Person groups")
    private List<Group> groupList;

    public PersonInfo() {
    }

    public PersonInfo(Person person, Address address, List<Group> groupList) {
        this.person = person;
        this.address = address;
        this.groupList = groupList;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Group> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<Group> groupList) {
        this.groupList = groupList;
    }


    @Override
    public String toString() {
        return "PersonAll{" +
                "person=" + person +
                ", address=" + address +
                ", groupList=" + groupList +
                '}';
    }
}
