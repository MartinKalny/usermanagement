package com.cgi.usermanagement.persistence.entities;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "PersonBasic", description = "Basic details about a person")
public class PersonBasic {
    @ApiModelProperty(value = "Person ID", example = "1")
    private Long id;
    @ApiModelProperty(value = "Person name", example = "name1")
    private String name;
    @ApiModelProperty(value = "Person email", example = "name1@email.com")
    private String email;

    public PersonBasic() {
    }

    public PersonBasic(Long id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "PersonBasic{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
