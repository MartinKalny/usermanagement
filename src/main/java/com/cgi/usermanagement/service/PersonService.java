package com.cgi.usermanagement.service;

import com.cgi.usermanagement.persistence.entities.PersonInfo;
import com.cgi.usermanagement.persistence.entities.PersonBasic;
import com.cgi.usermanagement.persistence.entities.Role;
import com.cgi.usermanagement.persistence.repository.PersonRepository;
import com.cgi.usermanagement.service.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PersonService {
    private PersonRepository personRepository;

    @Autowired
    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public PersonBasic getPersonById(Long id) {
        try {
            return personRepository.getPersonById(id);
        } catch (EmptyResultDataAccessException ex) {
            throw new ResourceNotFoundException("Person with id " + id + " was not found.", ex);
        }
    }

    public PersonBasic getPersonByEmail(String email) {
        try {
            return personRepository.getPersonByEmail(email);
        } catch (EmptyResultDataAccessException ex) {
            throw new ResourceNotFoundException("Person with email " + email + " was not found.", ex);
        }
    }

    public PersonInfo getPersonInfo(Long id) {
        try {
            return personRepository.getPersonInfo(id);
        } catch (EmptyResultDataAccessException ex) {
            throw new ResourceNotFoundException("Person with id " + id + " was not found.", ex);
        }
    }

    public List<Role> getRolesById(Long id) {
        try {
            return personRepository.getRolesById(id);
        } catch (EmptyResultDataAccessException ex) {
            throw new ResourceNotFoundException("Person with id " + id + " was not found.", ex);
        }
    }

}
