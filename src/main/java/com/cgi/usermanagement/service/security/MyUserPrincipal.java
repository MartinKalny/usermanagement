package com.cgi.usermanagement.service.security;

import com.cgi.usermanagement.persistence.entities.PersonInfo;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class MyUserPrincipal implements UserDetails {
    private PersonInfo person;

    public MyUserPrincipal(PersonInfo person) {
        this.person = person;
    }

    @Override
     public Collection<? extends GrantedAuthority> getAuthorities() {
        String auth;
        if(getManager()){
            auth = "manager";
        }else{
            auth = "user";
        }
        final List<GrantedAuthority> authorities = Collections.singletonList(new SimpleGrantedAuthority(auth));
        return authorities;
    }

    public boolean getManager(){
        return person.getGroupList().stream()
                .anyMatch(g ->g.getRole().getType().equals("manager"));
    }

    @Override
    public String getPassword() {
        return new String(person.getPerson().getPassword());
    }

    @Override
    public String getUsername() {
        return person.getPerson().getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
