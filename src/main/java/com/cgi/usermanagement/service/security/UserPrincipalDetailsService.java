package com.cgi.usermanagement.service.security;

import com.cgi.usermanagement.persistence.entities.PersonInfo;
import com.cgi.usermanagement.persistence.repository.PersonRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserPrincipalDetailsService implements UserDetailsService {
    private PersonRepository personRepository;

    public UserPrincipalDetailsService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        PersonInfo person = personRepository.getPersonInfoByEmail(email);
        MyUserPrincipal myUserPrincipal = new MyUserPrincipal(person);
        return myUserPrincipal;
    }
}
